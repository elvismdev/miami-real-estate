<?php

$rets_login_url = "http://www.dis.com:6103/rets/login";
$rets_username = "Joe";
$rets_password = "Schmoe";

//////////////////////////////

require_once("phrets.php");

// start rets connection
$rets = new phRETS;

// Uncomment and change the following if you're connecting
// to a server that supports a version other than RETS 1.5

//$rets->AddHeader("RETS-Version", "RETS/1.7.2");

echo "+ Connecting to {$rets_login_url} as {$rets_username}<br>\n";
$connect = $rets->Connect($rets_login_url, $rets_username, $rets_password);

// check for errors
if ($connect) {
        echo "  + Connected<br>\n";
}
else {
        echo "  + Not connected:<br>\n";
        print_r($rets->Error());
        exit;
}

$types = $rets->GetMetadataTypes();

// check for errors
if (!$types) {
        print_r($rets->Error());
}
else {
        foreach ($types as $type) {
                echo "+ Resource {$type['Resource']}<br>\n";

                foreach ($type['Data'] as $class) {
                        echo "  + Class {$class['ClassName']}<br>\n";
                }
        }
}

echo "+ Disconnecting<br>\n";
$rets->Disconnect();